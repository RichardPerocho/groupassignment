﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupAssignment.GameEngine
{
    class ComputerPlayer : Player
    {
        public enum Difficulty
        {
            easy,
            hard
        }

        public Difficulty LevelDifficulty { get; set; }

        public bool EasyDrawCheck()
        {
            if (RoundPoints > 18)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool HardDrawCheck(int card)
        {
            var cardValue = new Game().GetPointsValueOf(card, RoundPoints);
            if (RoundPoints + cardValue > 21)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
