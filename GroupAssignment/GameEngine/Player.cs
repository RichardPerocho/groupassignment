﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupAssignment.GameEngine
{
    abstract class Player
    {
        public enum EndStatus
        {
            won,
            lost
        }

        public List<int> CardsInHand { get; private set; }
        public EndStatus PlayerStatus { get; private set; }
        public int RoundLimit { get; private set; }
        public int GameScore { get; private set; }
        public int RoundPoints { get; private set; }

        public Player()
        {
            GameScore = 0;
            RoundPoints = 0;
        }

        public virtual void AddCardsToHand(int card)
        {
            CardsInHand.Add(card);
        }

        public virtual void SetPlayerStatus(EndStatus result)
        {
            PlayerStatus = result;
        }

        public virtual void SetRoundLimit(int limit)
        {
            RoundLimit = limit;
        }

        public virtual void AddGameScore(int gameScore)
        {
            GameScore += gameScore;
        }

        public virtual void AddRoundPoints(int roundPoints)
        {
            RoundPoints += roundPoints;
        }

        public virtual void ResetRoundPoints()
        {
            RoundPoints = 0;
        }
    }
}
