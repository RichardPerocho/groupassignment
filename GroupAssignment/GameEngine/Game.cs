﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupAssignment.GameEngine
{
    class Game
    {
        public List<int> CardDeck { get; private set; }
        public int RoundLimit { get; set; }
        private int[] DefaultDeck = Enumerable.Range(1, 52).ToArray();

        public Game()
        {
            CardDeck = DefaultDeck.ToList();
        }

        public void ShuffleDeck()
        {
            var DeckSize = CardDeck.Count;
            var r = new Random();

            while (DeckSize > 1)
            {
                int randomIndex = r.Next(DeckSize);
                DeckSize -= 1;
                int value = CardDeck[randomIndex];
                CardDeck[randomIndex] = CardDeck[DeckSize];
                CardDeck[DeckSize] = value;
            }
        }

        public int DrawCard()
        {
            var card = CardDeck[0];

            CardDeck.Remove(card);

            return card;
        }

        public int GetPointsValueOf(int card, int points)
        {
            int cardPoints = new int();

            if (1 <= card && card <= 4)
            {
                if (BustLoss(points + 11))
                {
                    cardPoints = 1;
                }
                else
                {
                    cardPoints = 11;
                }
            }
            else if (5 <= card && card <= 40)
            {
                cardPoints = 2;
                for (int i = 8; i < 40; i += 4)
                {
                    if (card <= i)
                    {
                        return cardPoints;
                    }
                    else
                    {
                        cardPoints++;
                    }
                }
            }
            else if (41 <= card && card <= 52)
            {
                cardPoints = 10;
            }

            return cardPoints;
        }

        public bool BustLoss(int points)
        {
            if (points <= 21)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void ReShuffleDeck()
        {
            CardDeck = DefaultDeck.ToList();
            ShuffleDeck();
        }

        public Player CheckRoundWinner(Player p1, Player p2)
        {
            if (p1.RoundPoints <= p2.RoundPoints || p1.RoundPoints > 21)
            {
                if (p2.RoundPoints > 21)
                {
                    return p1;
                }
                else
                {
                    return p2;

                }
            }
            else
            {
                return p1;
            }
        }

        public Player CheckGameWinner(Player p1, Player p2)
        {
            if (p1.GameScore == RoundLimit)
            {
                return p1;
            }
            else if (p2.GameScore == RoundLimit)
            {
                return p2;
            }
            else
            {
                return new HumanPlayer();
            }
        }
    }
}
