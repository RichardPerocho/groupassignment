﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupAssignment
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Cards : Page
    {
        public int VisibleCards { get; set; }
        public Cards()
        {
            this.InitializeComponent();
        }
        public void HideCards()
        {
            BackCard.Visibility = Visibility.Collapsed;
            CardAC.Visibility = Visibility.Collapsed;
            CardAD.Visibility = Visibility.Collapsed;
            CardAH.Visibility = Visibility.Collapsed;
            CardAS.Visibility = Visibility.Collapsed;
            Card2C.Visibility = Visibility.Collapsed;
            Card2D.Visibility = Visibility.Collapsed;
            Card2H.Visibility = Visibility.Collapsed;
            Card2S.Visibility = Visibility.Collapsed;
            Card3C.Visibility = Visibility.Collapsed;
            Card3D.Visibility = Visibility.Collapsed;
            Card3H.Visibility = Visibility.Collapsed;
            Card3S.Visibility = Visibility.Collapsed;
            Card4C.Visibility = Visibility.Collapsed;
            Card4D.Visibility = Visibility.Collapsed;
            Card4H.Visibility = Visibility.Collapsed;
            Card4S.Visibility = Visibility.Collapsed;
            Card5C.Visibility = Visibility.Collapsed;
            Card5D.Visibility = Visibility.Collapsed;
            Card5H.Visibility = Visibility.Collapsed;
            Card5S.Visibility = Visibility.Collapsed;
            Card6C.Visibility = Visibility.Collapsed;
            Card6D.Visibility = Visibility.Collapsed;
            Card6H.Visibility = Visibility.Collapsed;
            Card6S.Visibility = Visibility.Collapsed;
            Card7C.Visibility = Visibility.Collapsed;
            Card7D.Visibility = Visibility.Collapsed;
            Card7H.Visibility = Visibility.Collapsed;
            Card7S.Visibility = Visibility.Collapsed;
            Card8C.Visibility = Visibility.Collapsed;
            Card8D.Visibility = Visibility.Collapsed;
            Card8H.Visibility = Visibility.Collapsed;
            Card8S.Visibility = Visibility.Collapsed;
            Card9C.Visibility = Visibility.Collapsed;
            Card9D.Visibility = Visibility.Collapsed;
            Card9H.Visibility = Visibility.Collapsed;
            Card9S.Visibility = Visibility.Collapsed;
            Card10C.Visibility = Visibility.Collapsed;
            Card10D.Visibility = Visibility.Collapsed;
            Card10H.Visibility = Visibility.Collapsed;
            Card10S.Visibility = Visibility.Collapsed;
            CardJC.Visibility = Visibility.Collapsed;
            CardJD.Visibility = Visibility.Collapsed;
            CardJH.Visibility = Visibility.Collapsed;
            CardJS.Visibility = Visibility.Collapsed;
            CardKC.Visibility = Visibility.Collapsed;
            CardKD.Visibility = Visibility.Collapsed;
            CardKH.Visibility = Visibility.Collapsed;
            CardKS.Visibility = Visibility.Collapsed;
            CardQC.Visibility = Visibility.Collapsed;
            CardQD.Visibility = Visibility.Collapsed;
            CardQH.Visibility = Visibility.Collapsed;
            CardQS.Visibility = Visibility.Collapsed;

        }



        public void DisplayCard(int card)
        {
            HideCards();
            switch (card)
            {
                case 0:
                    BackCard.Visibility = Visibility.Visible;
                    break;
                case 1:
                    CardAC.Visibility = Visibility.Visible;
                    break;
                case 2:
                    CardAD.Visibility = Visibility.Visible;
                    break;
                case 3:
                    CardAH.Visibility = Visibility.Visible;
                    break;
                case 4:
                    CardAS.Visibility = Visibility.Visible;
                    break;
                case 5:
                    Card2C.Visibility = Visibility.Visible;
                    break;
                case 6:
                    Card2D.Visibility = Visibility.Visible;
                    break;
                case 7:
                    Card2H.Visibility = Visibility.Visible;
                    break;
                case 8:
                    Card2S.Visibility = Visibility.Visible;
                    break;
                case 9:
                    Card3C.Visibility = Visibility.Visible;
                    break;
                case 10:
                    Card3D.Visibility = Visibility.Visible;
                    break;
                case 11:
                    Card3H.Visibility = Visibility.Visible;
                    break;
                case 12:
                    Card3S.Visibility = Visibility.Visible;
                    break;
                case 13:
                    Card4C.Visibility = Visibility.Visible;
                    break;
                case 14:
                    Card4D.Visibility = Visibility.Visible;
                    break;
                case 15:
                    Card4H.Visibility = Visibility.Visible;
                    break;
                case 16:
                    Card4S.Visibility = Visibility.Visible;
                    break;
                case 17:
                    Card5C.Visibility = Visibility.Visible;
                    break;
                case 18:
                    Card5D.Visibility = Visibility.Visible;
                    break;
                case 19:
                    Card5H.Visibility = Visibility.Visible;
                    break;
                case 20:
                    Card5S.Visibility = Visibility.Visible;
                    break;
                case 21:
                    Card6C.Visibility = Visibility.Visible;
                    break;
                case 22:
                    Card6D.Visibility = Visibility.Visible;
                    break;
                case 23:
                    Card6H.Visibility = Visibility.Visible;
                    break;
                case 24:
                    Card6S.Visibility = Visibility.Visible;
                    break;
                case 25:
                    Card7C.Visibility = Visibility.Visible;
                    break;
                case 26:
                    Card7D.Visibility = Visibility.Visible;
                    break;
                case 27:
                    Card7H.Visibility = Visibility.Visible;
                    break;
                case 28:
                    Card7S.Visibility = Visibility.Visible;
                    break;
                case 29:
                    Card8C.Visibility = Visibility.Visible;
                    break;
                case 30:
                    Card8D.Visibility = Visibility.Visible;
                    break;
                case 31:
                    Card8H.Visibility = Visibility.Visible;
                    break;
                case 32:
                    Card8S.Visibility = Visibility.Visible;
                    break;
                case 33:
                    Card9C.Visibility = Visibility.Visible;
                    break;
                case 34:
                    Card9D.Visibility = Visibility.Visible;
                    break;
                case 35:
                    Card9H.Visibility = Visibility.Visible;
                    break;
                case 36:
                    Card9S.Visibility = Visibility.Visible;
                    break;
                case 37:
                    Card10C.Visibility = Visibility.Visible;
                    break;
                case 38:
                    Card10D.Visibility = Visibility.Visible;
                    break;
                case 39:
                    Card10H.Visibility = Visibility.Visible;
                    break;
                case 40:
                    Card10S.Visibility = Visibility.Visible;
                    break;
                case 41:
                    CardJC.Visibility = Visibility.Visible;
                    break;
                case 42:
                    CardJD.Visibility = Visibility.Visible;
                    break;
                case 43:
                    CardJH.Visibility = Visibility.Visible;
                    break;
                case 44:
                    CardJS.Visibility = Visibility.Visible;
                    break;
                case 45:
                    CardKC.Visibility = Visibility.Visible;
                    break;
                case 46:
                    CardKD.Visibility = Visibility.Visible;
                    break;
                case 47:
                    CardKH.Visibility = Visibility.Visible;
                    break;
                case 48:
                    CardKS.Visibility = Visibility.Visible;
                    break;
                case 49:
                    CardQC.Visibility = Visibility.Visible;
                    break;
                case 50:
                    CardQD.Visibility = Visibility.Visible;
                    break;
                case 51:
                    CardQH.Visibility = Visibility.Visible;
                    break;
                case 52:
                    CardQS.Visibility = Visibility.Visible;
                    break;
                
            }
        }

    }
}
