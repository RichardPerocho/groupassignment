﻿using GroupAssignment.GameEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupAssignment
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private int _roundCount { get; set; }
        private int _cardDrawLimit { get; set; }

        Game game = new Game();
        HumanPlayer HPlayer = new HumanPlayer();
        ComputerPlayer CPlayer = new ComputerPlayer();
        ComputerPlayer.Difficulty easy = ComputerPlayer.Difficulty.easy;
        ComputerPlayer.Difficulty hard = ComputerPlayer.Difficulty.hard;

        public MainPage()
        {
            this.InitializeComponent();


            _cardDrawLimit = game.CardDeck.Count;
            _roundCount = 0;

            game.ShuffleDeck();
            game.RoundLimit = 5;
            GameNotStarted();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            CPlayer.LevelDifficulty = (ComputerPlayer.Difficulty)e.Parameter;
        }

        private void DisablePlayerButtons()
        {
            HitBtn.IsEnabled = false;
            HoldBtn.IsEnabled = false;
            HitBtn_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/PageImages/HitDisabled.png"));
            HoldBtn_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/PageImages/HoldDisabled.png"));
        }

        private void EnablePlayerButtons()
        {
            HitBtn.IsEnabled = true;
            HoldBtn.IsEnabled = true;
            HitBtn_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/PageImages/Hit.png"));
            HoldBtn_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/PageImages/Hold.png"));
        }

        public void GameNotStarted()
        {
            RoundCountText.Visibility = Visibility.Collapsed;
            RoundCountNumber.Visibility = Visibility.Collapsed;
            BackBtn.Visibility = Visibility.Collapsed;
            ContinueBtn.Visibility = Visibility.Collapsed;
            HintBtn.Visibility = Visibility.Collapsed;
            DisablePlayerButtons();
        }

        private void StartGameButton(object sender, RoutedEventArgs e)
        {
            RoundCountText.Visibility = Visibility.Visible;
            RoundCountNumber.Visibility = Visibility.Visible;
            HintBtn.Visibility = Visibility.Visible;
            StartGameBtn.Visibility = Visibility.Collapsed;
            StartRound();

        }

        private void StartRound()
        {
            _roundCount += 1;
            RoundCountNumber.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{_roundCount}.png"));
            Computer_points.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/0.png"));
            AddPlayerStartingCards();
            EnablePlayerButtons();
        }

        private void AddPlayerStartingCards()
        {
            for (int i = 0; i < 2; i++)
            {
                int card = AddNewCard(HPlayer);
                HPlayer.AddRoundPoints(game.GetPointsValueOf(card, HPlayer.RoundPoints));
                Player_points.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{HPlayer.RoundPoints}.png"));
            }
        }

        private int AddNewCard(Player player)
        {
            var display = new StackPanel();
            if (player == HPlayer)
            {
                display = PlayerCards;
            }
            else
            {
                display = ComputerCards;
            }
            int card = game.DrawCard();
            _cardDrawLimit -= 1;
            Cards newCard = new Cards();
            display.Children.Add(newCard);

            newCard.DisplayCard(card);
            newCard.Margin = new Thickness(-30, 0, 0, 0);
            newCard.RenderTransform = new RotateTransform() { Angle = -3 };

            return card;
        }

        public void StartComputerTurn()
        {
            for (int i = 0; i < 15; i++)
            {
                if (CPlayer.LevelDifficulty == hard && CPlayer.HardDrawCheck(game.CardDeck[0]) ||
                    CPlayer.LevelDifficulty == easy && CPlayer.EasyDrawCheck())
                {
                    int cardValue = game.GetPointsValueOf(AddNewCard(CPlayer), CPlayer.RoundPoints);
                    CPlayer.AddRoundPoints(cardValue);
                    Computer_points.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{CPlayer.RoundPoints}.png"));
                }
                else
                {
                    RoundEnd();
                    break;
                }
            }
        }

        private async void RoundEnd()
        {
            var RoundWinner = game.CheckRoundWinner(HPlayer, CPlayer);
            if (RoundWinner == CPlayer)
            {
                CPlayer.AddGameScore(1);
                ComputerGameScore.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{CPlayer.GameScore}.png"));
            }
            else
            {
                HPlayer.AddGameScore(1);
                PlayerGameScore.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{HPlayer.GameScore}.png"));
            }
            var gameWinner = game.CheckGameWinner(HPlayer, CPlayer);
            if (gameWinner == HPlayer)
            {
                await new MessageDialog("You Won!").ShowAsync();
                BackBtn.Visibility = Visibility.Visible;
            }
            else if (gameWinner == CPlayer)
            {
                await new MessageDialog("You Lost...").ShowAsync();
                BackBtn.Visibility = Visibility.Visible;
            }
            else
            {
                ContinueBtn.Visibility = Visibility.Visible;
            }
            DisablePlayerButtons();
        }

        private void HitHandButton(object sender, RoutedEventArgs e)
        {
            if (_cardDrawLimit <= 0)
            {
                DisablePlayerButtons();
            }
            int card = AddNewCard(HPlayer);
            int hit_points = game.GetPointsValueOf(card, HPlayer.RoundPoints);
            HPlayer.AddRoundPoints(hit_points);
            Player_points.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Numbers/{HPlayer.RoundPoints}.png"));

            if (game.BustLoss(HPlayer.RoundPoints))
            {
                RoundEnd();
            }

        }

        private void HoldButton(object sender, RoutedEventArgs e)
        {
            StartComputerTurn();
        }

        private void BackButton(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(StartPage));
        }

        private void NextRoundButton(object sender, RoutedEventArgs e)
        {
            RoundCountText.Visibility = Visibility.Visible;
            RoundCountNumber.Visibility = Visibility.Visible;
            ContinueBtn.Visibility = Visibility.Collapsed;

            HPlayer.ResetRoundPoints();
            CPlayer.ResetRoundPoints();

            game.ReShuffleDeck();
            _cardDrawLimit = game.CardDeck.Count;

            PlayerCards.Children.Clear();
            ComputerCards.Children.Clear();

            StartRound();
        }

        private async void HintButton(object sender, RoutedEventArgs e)
        {
            string message;
            int points = game.GetPointsValueOf(game.CardDeck[0], HPlayer.RoundPoints);
            if (points <= 4)
            {
                message = "A low card will be drawn (points value 0 to 4)";
            }
            else if (points <= 7)
            {
                message = "A medium card will be drawn (points value 5 to 7)";
            }
            else
            {
                message = "A high card will be drawn (points value 8 to 10)";
            }

            MessageDialog hint = new MessageDialog(message);
            HintBtn.IsEnabled = false;
            HintBtn.Visibility = Visibility.Collapsed;
            await hint.ShowAsync();
        }
    }
}
